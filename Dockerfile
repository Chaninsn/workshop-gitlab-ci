# Load migrate app Stage
FROM alpine:latest as migrate
WORKDIR /app
RUN apk --no-cache add curl
RUN curl -L https://github.com/golang-migrate/migrate/releases/download/v4.15.2/migrate.linux-amd64.tar.gz | tar xvz

FROM node:16-alpine
ENV NODE_ENV=production
WORKDIR /app
# Expose ports (for orchestrators and dynamic reverse proxies)
EXPOSE 3000

# Install deps for production only
COPY ./package* ./
RUN npm ci && \
  npm cache clean --force
# Copy builded source from the upper builder stage
COPY ./migrations ./migrations
COPY ./src ./src
COPY --from=migrate /app/migrate ./migrate

# Start the app
CMD ["node", "src/index.js"]
